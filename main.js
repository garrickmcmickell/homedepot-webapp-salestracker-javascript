var rowCount = 0;
var entryTable;
            
function initialize() 
{   
    entryTable = document.getElementById("entries");
    createRow();
}

function createRow() 
{
    var row = entryTable.insertRow(rowCount);
    var cell0 = entryTable.rows[rowCount].insertCell(0);
    var cell1 = entryTable.rows[rowCount].insertCell(1);
    var cell2 = entryTable.rows[rowCount].insertCell(2);
    var cell3 = entryTable.rows[rowCount].insertCell(3);
    cell0.innerHTML = "<h3 class=\"contactNum\">" + (rowCount + 1) + "</h3>";
    cell1.innerHTML = "<h3>$&nbsp;&nbsp;</h3><input class=\"entryField salesEntry\" type=\"text\" value=\"0.00\" onchange=\"updateTotal()\">";
    cell2.innerHTML = "<input class=\"entryField npsEntry\" type=\"text\" value=\"0\" onchange=\"updateTotal()\">";
    cell3.innerHTML = "<input class=\"button delete\" type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\">";
    rowCount++;
    updateTotal();
    
    window.scrollTo(0, document.body.scrollHeight);
}

function updateTotal()
{
    var total = 0;
    var npsTotal = 0;
    var npsCount = 0;

    for(var i = 0; i < rowCount; i++)
    {
        entryTable.rows[i].cells[0].children[0].innerHTML = (i + 1);
        entryTable.rows[i].cells[0].children[0].value = (i + 1);
        
        if(entryTable.rows[i].cells[0].children[0].innerHTML % 2 == 0)
        {
            entryTable.rows[i].style = "background-color: #ffdb99;"
        }
        else
        {
            entryTable.rows[i].style = "background-color: transparent;"
        }
        
        if(isNaN(entryTable.rows[i].cells[1].children[1].value) === true ||           entryTable.rows[i].cells[1].children[1].value == "" ||     entryTable.rows[i].cells[1].children[1].value < 0) 
        {
            entryTable.rows[i].cells[1].children[1].value = "0.00";
        } 
        else 
        {
            total += parseFloat(entryTable.rows[i].cells[1].children[1].value);
            entryTable.rows[i].cells[1].children[1].value = parseFloat(entryTable.rows[i].cells[1].children[1].value).toFixed(2);
        }
        
        if(isNaN(entryTable.rows[i].cells[2].children[0].value) === true ||           entryTable.rows[i].cells[2].children[0].value == "" ||     entryTable.rows[i].cells[2].children[0].value < 0 || entryTable.rows[i].cells[2].children[0].value > 5) 
        {
            entryTable.rows[i].cells[2].children[0].value = "0";
        }
        else 
        {
            if(entryTable.rows[i].cells[2].children[0].value > 0) 
            {
                npsCount++;
                
                if (entryTable.rows[i].cells[2].children[0].value >= 4) 
                {
                    npsTotal++;
                }
                if (entryTable.rows[i].cells[2].children[0].value <= 2) 
                {
                    npsTotal--;
                }
            }
        }
    }
    
    document.getElementById("sales").innerHTML = "$" + total.toFixed(2);
    document.getElementById("rpc").innerHTML = "$" + (total / rowCount).toFixed(2);
    if (npsCount == 0) 
    {
        document.getElementById("nps").innerHTML = "0.00%";
    }
    else 
    {
        document.getElementById("nps").innerHTML = ((npsTotal / npsCount) * 100).toFixed(2) + "%";
    }
    document.getElementById("takerate").innerHTML = ((npsCount / rowCount) * 100).toFixed(2) + "%";
}

function deleteRow(button) 
{
    var curRow = button.parentNode.parentNode;
    curRow.parentNode.removeChild(curRow);
    rowCount--;
    updateTotal();
}

function changeToDark()
{
    document.getElementById("style").href = "dark.css";
}

function changeToLight() 
{
    document.getElementById("style").href = "light.css";
}
